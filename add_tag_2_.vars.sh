#!/bin/sh
DOCKER_COMPOSE_FILE="$1"
IMAGE="$2"
VAR_NAME="$3"
FILE=".vars"

usage() {
    echo "One argument is missing. Please use:"
    echo "$0 <path/to/docker-compose.yml> <image name to search> <var name>"
}

[ -z "$DOCKER_COMPOSE_FILE" ] || [ -z "$IMAGE" ] || [ -z "$VAR_NAME" ]  && usage && exit 1


# Check if target file exists, if not add header
if test -f ${FILE}
then
    echo ""
else
    echo "#!/bin/sh" > ${FILE}

fi

# Add ENV VAR
echo "export ${VAR_NAME}=\"$(grep "image:.*${IMAGE}.*" "${DOCKER_COMPOSE_FILE}" | cut -d : -f 3|uniq|head -n 1)\"" >> ${FILE}
